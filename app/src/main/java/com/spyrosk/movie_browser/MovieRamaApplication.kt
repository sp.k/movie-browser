package com.spyrosk.movie_browser

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MovieBrowserApplication : Application()