package com.spyrosk.movie_browser.repository

import android.content.Context
import android.util.Log
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringSetPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "PreferenceRepository"

@Singleton
class PreferenceRepository @Inject constructor(@ApplicationContext private val context: Context) {
	
	private val Context.dataStore by preferencesDataStore("app_preferences")
	
	private object PreferenceKeys {
		val FAVORITE_MOVIES_KEY = stringSetPreferencesKey("FAVORITE_MOVIES")
	}
	
	val favoriteMoviesFlow = context.dataStore.data
		.catch { e -> handleReadException(e) }
		.map { preferences -> preferences[PreferenceKeys.FAVORITE_MOVIES_KEY] ?: emptySet() }
	
	private suspend fun FlowCollector<Preferences>.handleReadException(e: Throwable) {
		if(e is IOException) {
			Log.e(TAG, "IOException when reading preferences", e)
			emit(emptyPreferences())
		}
		else {
			throw e
		}
	}
	
	suspend fun addFavorite(movieId: String) {
		context.dataStore.edit { preferences ->
			val oldSet = preferences[PreferenceKeys.FAVORITE_MOVIES_KEY]
			
			val newSet = oldSet?.let { HashSet(it) } ?: HashSet()
			newSet.add(movieId)
			preferences[PreferenceKeys.FAVORITE_MOVIES_KEY] = newSet
		}
	}
	
	suspend fun removeFavorite(movieId: String) {
		context.dataStore.edit { preferences ->
			val oldSet = preferences[PreferenceKeys.FAVORITE_MOVIES_KEY]
			
			val newSet = oldSet?.let { HashSet(it) } ?: HashSet()
			newSet.remove(movieId)
			preferences[PreferenceKeys.FAVORITE_MOVIES_KEY] = newSet
		}
	}
	
}
