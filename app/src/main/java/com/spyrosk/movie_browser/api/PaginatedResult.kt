package com.spyrosk.movie_browser.api

import com.squareup.moshi.Json

data class PaginatedResult<T>(
	val page: Int,
	val results: List<T>,
	@Json(name = "total_pages") val totalPages: Int,
	@Json(name = "total_results") val totalResults: Int
)
