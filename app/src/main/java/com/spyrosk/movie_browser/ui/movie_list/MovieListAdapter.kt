package com.spyrosk.movie_browser.ui.movie_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.spyrosk.movie_browser.api.ImageType
import com.spyrosk.movie_browser.data.Movie
import com.spyrosk.movie_browser.api.ImageUrlCreator
import com.spyrosk.movie_browser.data.MovieWithFavoriteStatus
import com.spyrosk.movie_browser.databinding.ItemMovieBinding
import com.spyrosk.movie_browser.utils.safeLoadWithGlide

class MovieListAdapter(
	private val imageUrlCreator: ImageUrlCreator,
	private val onMovieClickListener: OnMovieClickListener,
	private val onMovieFavoriteClickListener: OnMovieFavoriteClickListener
) : ListAdapter<MovieWithFavoriteStatus, MovieListAdapter.ViewHolder>(MovieDiffCallback()) {
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
		return ViewHolder(binding)
	}
	
	override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))
	
	fun interface OnMovieClickListener {
		fun onMovieClicked(movie: Movie)
	}
	
	fun interface OnMovieFavoriteClickListener {
		fun onMovieClicked(movie: Movie, buttonView: View)
	}
	
	inner class ViewHolder(private val itemMovieBinding: ItemMovieBinding) : RecyclerView.ViewHolder(itemMovieBinding.root) {
		
		init {
			itemMovieBinding.root.setOnClickListener {
				if(adapterPosition != RecyclerView.NO_POSITION)
					onMovieClickListener.onMovieClicked(getItem(adapterPosition).movie)
			}
			
			itemMovieBinding.btnFavorite.setOnClickListener {
				if(adapterPosition != RecyclerView.NO_POSITION)
					onMovieFavoriteClickListener.onMovieClicked(getItem(adapterPosition).movie, itemMovieBinding.btnFavorite)
			}
		}
		
		fun bind(movieWithFavorite: MovieWithFavoriteStatus) {
			val movie = movieWithFavorite.movie
			with(itemMovieBinding) {
				tvMovieTitle.text = movie.title
				tvMovieReleaseDate.text = movie.releaseDate
				movie.ratingOutOfTen?.let { rtbRating.rating = movie.ratingOutOfTen / 2 }
				
				btnFavorite.isChecked = movieWithFavorite.isFavorite
				
				imgMoviePoster.safeLoadWithGlide(root, imageUrlCreator, movie.posterRelativePath, ImageType.POSTER)
			}
		}
		
	}
	
}

class MovieDiffCallback : DiffUtil.ItemCallback<MovieWithFavoriteStatus>() {
	
	override fun areItemsTheSame(oldItem: MovieWithFavoriteStatus, newItem: MovieWithFavoriteStatus): Boolean = (oldItem.movie.id == newItem.movie.id)
	
	// Ignores changes in favorite status
	override fun areContentsTheSame(oldItem: MovieWithFavoriteStatus, newItem: MovieWithFavoriteStatus): Boolean = (oldItem.movie == newItem.movie)
	
}

