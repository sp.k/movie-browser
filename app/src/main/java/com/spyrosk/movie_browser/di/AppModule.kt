package com.spyrosk.movie_browser.di

import android.content.Context
import android.net.ConnectivityManager
import com.spyrosk.movie_browser.AppConfig
import com.spyrosk.movie_browser.BuildConfig
import com.spyrosk.movie_browser.api.MovieApiService
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
	
	@Provides
	@Singleton
	fun provideAppConfig(): AppConfig = AppConfig(
		apiKey = BuildConfig.API_KEY,
		apiBaseUrl = BuildConfig.API_BASE_URL,
		imageBaseUrl = BuildConfig.IMAGE_BASE_URL
	)
	
	@Provides
	@Singleton
	fun provideMoshi(): Moshi = Moshi.Builder()
		.add(KotlinJsonAdapterFactory())
		.build()
	
	@Provides
	@Singleton
	fun provideApiClient(moshi: Moshi, appConfig: AppConfig): Retrofit {
		val apiKeyInterceptor = Interceptor { chain ->
			val originalRequest = chain.request()
			val originalUrl = originalRequest.url()
			
			val newUrl =
				originalUrl.newBuilder().addQueryParameter("api_key", appConfig.apiKey).build()
			val newRequest = originalRequest.newBuilder().url(newUrl).build()
			
			chain.proceed(newRequest)
		}
		
		val client = OkHttpClient.Builder()
			.addInterceptor(apiKeyInterceptor)
			.callTimeout(5, TimeUnit.SECONDS)
			.connectTimeout(5, TimeUnit.SECONDS)
			.readTimeout(5, TimeUnit.SECONDS)
			.writeTimeout(5, TimeUnit.SECONDS)
			.retryOnConnectionFailure(false)
			.build()
		
		return Retrofit.Builder()
			.client(client)
			.addConverterFactory(MoshiConverterFactory.create(moshi))
			.baseUrl(appConfig.apiBaseUrl)
			.build()
	}
	
	@Provides
	@Singleton
	fun provideMovieApiService(retrofit: Retrofit): MovieApiService =
		retrofit.create(MovieApiService::class.java)
	
	@Provides
	fun provideConnectivityManager(@ApplicationContext context: Context) =
		 context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
	
}
